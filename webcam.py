import numpy as np
import face_recognition as fr
import cv2
import time

video_capture = cv2.VideoCapture(0)
#video_capture.set(cv2.CAP_PROP_FOURCC, cv2.VideoWriter.fourcc('m','j','p','g'))
#video_capture.set(cv2.CAP_PROP_FRAME_WIDTH, 50);
#video_capture.set(cv2.CAP_PROP_FRAME_HEIGHT, 50);

people_1 = fr.load_image_file("KnownPeoples/German.jpg")
people_2 = fr.load_image_file("KnownPeoples/Nekit.png")
people_3 = fr.load_image_file("KnownPeoples/Sanya.png")
people_4 = fr.load_image_file("KnownPeoples/Ilnyr.png")
people_5 = fr.load_image_file("KnownPeoples/Kolya.png")

German_face_encoding = fr.face_encodings(people_1)[0]
Nekit_face_encoding = fr.face_encodings(people_2)[0]
Sanya_face_encoding = fr.face_encodings(people_3)[0]
Ilnyr_face_encoding = fr.face_encodings(people_4)[0]
Kolya_face_encoding = fr.face_encodings(people_5)[0]

known_face_encondings = [German_face_encoding, Nekit_face_encoding, Sanya_face_encoding, Ilnyr_face_encoding, Kolya_face_encoding]
known_face_names = ["German", "Nikita" , "Sasha", "Ilnyr", "Kolya"]

while True:
    ret, frame = video_capture.read()
    #time.sleep(0.001)

    rgb_frame = frame[:, :, ::-1]

    face_locations = fr.face_locations(rgb_frame)
    face_encodings = fr.face_encodings(rgb_frame, face_locations)

    for (top, right, bottom, left), face_encoding in zip(face_locations, face_encodings):

        matches = fr.compare_faces(known_face_encondings, face_encoding)

        name = "Unknown"

        face_distances = fr.face_distance(known_face_encondings, face_encoding)

        best_match_index = np.argmin(face_distances)
        if matches[best_match_index]:
            name = known_face_names[best_match_index]

        cv2.rectangle(frame, (left - 10, top), (right + 10, bottom + 25), (0, 0, 255), 2)

        cv2.rectangle(frame, (left - 10, bottom - 5), (right + 10, bottom + 25), (0, 0, 255), cv2.FILLED)
        font = cv2.FONT_HERSHEY_SIMPLEX
        cv2.putText(frame, name, (left + 6, bottom + 20), font, 1.0, (255, 255, 255), 1)

    cv2.imshow('GERMAN_FaceID', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

video_capture.release()
cv2.destroyAllWindows()